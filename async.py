import aiohttp
import asyncio

async def fetch_data(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            data = await response.json()
            return data

async def main():
    api_url = 'https://jsonplaceholder.typicode.com/users'

    try:
        data = await fetch_data(api_url)
        # Обработка полученных данных
        for user in data:
            print(f'User ID: {user["id"]}')
            print(f'Name: {user["name"]}')
            print(f'Email: {user["email"]}')
            print('---')
    except aiohttp.ClientError as e:
        print("Ошибка при выполнении запроса:", str(e))

if __name__ == "__main__":
    asyncio.run(main())
